import axios_ from "axios";
import store from "./store";

let axios = axios_.create({
  baseURL: "http://localhost:" + process.env.appPort + "/api",
  timeout: 5000,
  validateStatus: null
});

axios.interceptors.response.use(response => {
  let { auth } = store;
  if (response.status === 401) {
    auth.auth = "false";
  }
  return response;
});

export default axios;
