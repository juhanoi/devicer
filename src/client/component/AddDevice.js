import React from "react";
import { Link } from "react-router-dom";
import { observable } from "mobx";
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  TextField,
  Radio,
  RadioGroup,
  FormHelperText,
  FormLabel,
  FormControlLabel,
  FormControl
} from "@material-ui/core";
import { observer, inject } from "mobx-react";
import axios from "../axios";
@inject("device")
@observer
class AddDevice extends React.Component {
  async componentDidMount() {
    let { props } = this;
    let data = (await axios.get("device")).data;
    props.device.table = data;
  }
  render() {
    let { device } = this.props;
    return (
      <div>
        <h1>Add Device</h1>
      </div>
    );
  }
}
export default AddDevice;
