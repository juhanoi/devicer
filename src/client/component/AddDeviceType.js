import React from "react";
import { Link } from "react-router-dom";
import { observable } from "mobx";
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  TextField,
  Radio,
  RadioGroup,
  FormHelperText,
  FormLabel,
  FormControlLabel,
  FormControl
} from "@material-ui/core";
import { observer, inject } from "mobx-react";
import axios from "../axios";
@observer
class AddDeviceType extends React.Component {
  @observable value = "";
  render() {
    return (
      <div>
        <h1>Add Device Type</h1>
        <form>
          <TextField
            name="value"
            label="value"
            onChange={event => {
              this.value = event.target.value;
            }}
            value={this.value}
          />
          <Button
            variant="raised"
            color="primary"
            onClick={async () => {
              await axios.post("device_type", {
                value: this.value
              });
            }}
          >
            Submit
          </Button>
        </form>
      </div>
    );
  }
}
export default AddDeviceType;
