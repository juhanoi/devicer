import React from "react";
import PropTypes from "prop-types";
import { Button, IconButton, Dialog, DialogTitle } from "@material-ui/core";

export let ConfirmDialog = ({ title, label, onConfirm, ...other }) => (
  <Dialog {...other}>
    <DialogTitle>{title}</DialogTitle>
    <Button
      color="secondary"
      variant="raised"
      autoFocus
      onClick={e => {
        onConfirm();
        other.onClose(e);
      }}
    >
      {label || "confirm"}
    </Button>
  </Dialog>
);

ConfirmDialog.propTypes = {
  title: PropTypes.string.isRequired,
  onConfirm: PropTypes.func.isRequired,
  label: PropTypes.string
};

export default ConfirmDialog;
