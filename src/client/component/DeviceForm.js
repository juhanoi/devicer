import React from "react";
import { observer } from "mobx-react";
import {
  Button,
  TextField,
  Radio,
  RadioGroup,
  FormHelperText,
  FormLabel,
  FormControlLabel,
  FormControl,
  Checkbox,
  Typography,
  Select,
  MenuItem,
  InputLabel
} from "@material-ui/core";
import * as icon from "@material-ui/icons";
import MobxReactForm from "mobx-react-form";
import Ajv from "ajv";
import schemas from "../../common/schemas";
import FormDialog from "./FormDialog";

let form = new MobxReactForm(
  { schema: schemas.device },
  { plugins: { svk: Ajv } }
);

@observer
export default class DeviceForm extends React.Component {
  render() {
    let { ...other } = this.props;
    return (
      <FormDialog title="device" {...other}>
        <div>
          <TextField
            name={form.$("profile_number").id}
            label={form.$("profile_number").label}
            onChange={form.$("profile_number").onChange}
          />
          <p>{form.$("profile_number").error}</p>

          <TextField
            name={form.$("label").id}
            label={form.$("label").label}
            onChange={form.$("label").onChange}
          />
          <p>{form.$("label").error}</p>

          <TextField
            name={form.$("make_model").id}
            label={form.$("make_model").label}
            onChange={form.$("make_model").onChange}
          />
          <p>{form.$("make_model").error}</p>

          <TextField
            name={form.$("serial_number").id}
            label={form.$("serial_number").label}
            onChange={form.$("serial_number").onChange}
          />
          <p>{form.$("serial_number").error}</p>

          <TextField
            name={form.$("location").id}
            label={form.$("location").label}
            onChange={form.$("location").onChange}
          />
          <p>{form.$("location").error}</p>

          <TextField
            name={form.$("additional_info").id}
            label={form.$("additional_info").label}
            onChange={form.$("additional_info").onChange}
          />
          <p>{form.$("additional_info").error}</p>

          <TextField
            name={form.$("ip_address").id}
            label={form.$("ip_address").label}
            onChange={form.$("ip_address").onChange}
          />
          <p>{form.$("ip_address").error}</p>

          <TextField
            name={form.$("cpu").id}
            label={form.$("cpu").label}
            onChange={form.$("cpu").onChange}
          />
          <p>{form.$("cpu").error}</p>

          <TextField
            name={form.$("ram").id}
            label={form.$("ram").label}
            onChange={form.$("ram").onChange}
          />
          <p>{form.$("ram").error}</p>

          <TextField
            name={form.$("hdd").id}
            label={form.$("hdd").label}
            onChange={form.$("hdd").onChange}
          />
          <p>{form.$("hdd").error}</p>

          <TextField
            name={form.$("gpu").id}
            label={form.$("gpu").label}
            onChange={form.$("gpu").onChange}
          />
          <p>{form.$("gpu").error}</p>

          <TextField
            name={form.$("purchase_store").id}
            label={form.$("purchase_store").label}
            onChange={form.$("purchase_store").onChange}
          />
          <p>{form.$("purchase_store").error}</p>

          <TextField
            name={form.$("purchase_date").id}
            label={form.$("purchase_date").label}
            onChange={form.$("purchase_date").onChange}
            type="date"
          />
          <p>{form.$("purchase_date").error}</p>

          <p>{form.error}</p>
        </div>
      </FormDialog>
    );
  }
}
