import React from "react";
import { Link } from "react-router-dom";
import { observable } from "mobx";
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper
} from "@material-ui/core";
import { observer, inject } from "mobx-react";
import axios from "../axios";

@inject("deviceSoftware")
@observer
class DeviceSoftwares extends React.Component {
  deviceSoftware = this.props.deviceSoftware;
  async componentDidMount() {
    let data = (await axios.get("device_software")).data;
    this.deviceSoftware.table = data.data;
  }
  render() {
    let { table } = this.props.deviceSoftware;
    return (
      <div>
        <h1>Device software</h1>
        <Button variant="raised">add device software</Button>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>id</TableCell>
                <TableCell>device</TableCell>
                <TableCell>software</TableCell>
                <TableCell>info</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.deviceSoftware.table.map(a => (
                <TableRow key={a.id}>
                  <TableCell>{a.device_id}</TableCell>
                  <TableCell>{a.software_id}</TableCell>
                  <TableCell>{a.license_key}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

export default DeviceSoftwares;
