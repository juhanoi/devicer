import React from "react";
import { Link } from "react-router-dom";
import { observable } from "mobx";
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper
} from "@material-ui/core";
import { observer, inject } from "mobx-react";
import axios from "../axios";

@inject("deviceType")
@observer
class DeviceTypes extends React.Component {
  deviceType = this.props.deviceType;
  async componentDidMount() {
    let data = (await axios.get("device_type")).data;
    this.deviceType.table = data.data;
  }
  render() {
    return (
      <div>
        <h1>Device type</h1>
        <Button variant="raised" component={Link} to="/device_type/add">
          add device type
        </Button>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>id</TableCell>
                <TableCell>value</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.deviceType.table.map(a => (
                <TableRow key={a.id}>
                  <TableCell>{a.id}</TableCell>
                  <TableCell>{a.value}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

export default DeviceTypes;
