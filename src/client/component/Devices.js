import React from "react";
import { Link } from "react-router-dom";
import { observable } from "mobx";
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper
} from "@material-ui/core";
import { observer, inject } from "mobx-react";
import axios from "../axios";

@inject("device")
@observer
class Devices extends React.Component {
  async componentDidMount() {
    let { props } = this;
    let data = (await axios.get("device")).data;
    if (data.error) {
      throw data.error;
    }
    props.device.table = data.data;
  }
  render() {
    let { device } = this.props;
    return (
      <div>
        <h1>Devices</h1>
        <Button variant="raised" component={Link} to="/device/add">
          add device
        </Button>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>id</TableCell>
                <TableCell>label</TableCell>
                <TableCell>manufacturer_and_model</TableCell>
                <TableCell>device_type</TableCell>
                <TableCell>owner</TableCell>
                <TableCell>info</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {device.table.map(a => (
                <TableRow key={a.id}>
                  <TableCell>{a.id}</TableCell>
                  <TableCell>{a.label}</TableCell>
                  <TableCell>{a.manufacturer_and_model}</TableCell>
                  <TableCell>{a.device_type}</TableCell>
                  <TableCell>{a.owner}</TableCell>
                  <TableCell>{a.info}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

export default Devices;
