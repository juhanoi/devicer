import React from "react";
import { observer } from "mobx-react";
import PropTypes from "prop-types";
import {
  Button,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogContentText
} from "@material-ui/core";

@observer
export default class FormDialog extends React.Component {
  render() {
    let { title, children, ...other } = this.props;
    return (
      <Dialog {...other}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>nice form</DialogContentText>
          {children}
        </DialogContent>
        <DialogActions>
          <Button
            variant="raised"
            color="secondary"
            onClick={e => {
              other.onClose(e);
            }}
          >
            cancel
          </Button>
          <Button variant="contained" color="primary">
            ok
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

FormDialog.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired
};
