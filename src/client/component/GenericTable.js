import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { toJS, observable } from "mobx";
import {
  Grid,
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  TextField,
  Radio,
  RadioGroup,
  FormHelperText,
  FormLabel,
  FormControlLabel,
  FormControl,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  FormGroup,
  Typography,
  Select,
  MenuItem,
  InputLabel
} from "@material-ui/core";
import * as icon from "@material-ui/icons";
import { withStyles } from "@material-ui/core/styles";
import { observer, inject, PropTypes as MobxPropTypes } from "mobx-react";
import axios from "../axios";
import ConfirmDialog from "./ConfirmDialog";

@withStyles(theme => ({}))
@observer
class GenericTable extends React.Component {
  @observable
  visibleColumns = Object.entries(this.props.schema.properties).reduce(
    (a, [k, v]) => ({
      ...a,
      ...{ [k]: !(v.hide_column || false) }
    }),
    {}
  );
  @observable editDialogOpen = false;
  @observable deleteDialogOpen = false;
  @observable dialogId = null;

  async updateTable() {
    let { table, store } = this.props;
    let data = (await axios.get(table)).data;
    if (data.error) {
      throw data.error;
    }
    store.table = data.data;
    console.log("update table " + table, data.data, toJS(this.visibleColumns));
  }

  async componentDidMount() {
    this.updateTable();
  }

  render() {
    let { table, store, schema, Form } = this.props;
    return (
      <div>
        <Typography variant="title">{schema.title}</Typography>
        <ConfirmDialog
          title={"Delete row?"}
          label={"delete"}
          open={this.deleteDialogOpen}
          onClose={e => {
            this.deleteDialogOpen = false;
          }}
          onConfirm={async () => {
            await axios.delete(table + "/" + this.dialogId);
            await this.updateTable();
          }}
        />
        <Grid container spacing={24}>
          <Grid item xs={2}>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                this.editDialogOpen = true;
              }}
            >
              <icon.Add /> add {schema.title}
            </Button>
            <Form
              open={this.editDialogOpen}
              onClose={e => {
                this.editDialogOpen = false;
              }}
            />
          </Grid>
          <Grid item xs>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                this.updateTable();
              }}
            >
              <icon.Refresh /> refresh
            </Button>
          </Grid>
          <Grid item xs>
            <TextField name="value" label="haku" />
            <FormControl>
              <InputLabel>column</InputLabel>
              <Select value="">
                {Object.keys(schema.properties).map(k => (
                  <MenuItem key={k} value={k}>
                    {k}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        </Grid>
        <Grid container spacing={24}>
          <Grid item xs={2}>
            <Paper>
              <FormControl component="fieldset">
                <FormLabel component="legend">Visible columns</FormLabel>
                <FormGroup>
                  {Object.entries(schema.properties).map(([k, v]) => {
                    let label = v.description || k;
                    return (
                      <FormControlLabel
                        key={k}
                        control={
                          <Checkbox
                            value={k}
                            checked={this.visibleColumns[k]}
                            onChange={() => {
                              this.visibleColumns[k] = !this.visibleColumns[k];
                            }}
                          />
                        }
                        label={label}
                      />
                    );
                  })}
                </FormGroup>
              </FormControl>
            </Paper>
          </Grid>
          <Grid item xs>
            <Paper>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell style={{ width: 0 }} />
                    {Object.entries(schema.properties).map(
                      ([k, v]) =>
                        this.visibleColumns[k] && (
                          <TableCell key={k}>{v.description || k}</TableCell>
                        )
                    )}
                    <TableCell style={{ width: 0 }} />
                  </TableRow>
                </TableHead>
                <TableBody>
                  {store.table.map(row => (
                    <TableRow key={row.id}>
                      <TableCell>
                        <Button variant="fab" color="primary" mini>
                          <icon.Edit />
                        </Button>
                      </TableCell>
                      {Object.entries(row).map(
                        ([k, v]) =>
                          this.visibleColumns[k] && (
                            <TableCell key={k}>{v}</TableCell>
                          )
                      )}
                      <TableCell>
                        <Button
                          variant="fab"
                          color="secondary"
                          mini
                          onClick={() => {
                            this.dialogId = row.id;
                            this.deleteDialogOpen = true;
                          }}
                        >
                          <icon.Delete />
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

GenericTable.propTypes = {
  table: PropTypes.string.isRequired,
  schema: PropTypes.object.isRequired,
  store: MobxPropTypes.observableObject.isRequired
};

export default GenericTable;
