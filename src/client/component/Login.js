import React from "react";
import { observable } from "mobx";
import {
  Paper,
  Input,
  InputLabel,
  InputAdornment,
  FormControl,
  IconButton,
  Button
} from "@material-ui/core";
import { observer, inject } from "mobx-react";
import * as icon from "@material-ui/icons";
import axios from "../axios";

@inject("auth")
@observer
class Login extends React.Component {
  @observable username = "";
  @observable password = "";
  @observable showPassword = false;
  render() {
    return (
      <div>
        <h1>Digitalents Device Registry</h1>
        <Paper>
          <form
            onSubmit={async e => {
              e.preventDefault();
              let res = (await axios.post("login", {
                username: this.username,
                password: this.password
              })).data;
              if (res.error) {
                throw res.error;
              }
              this.props.auth.auth = "true";
            }}
          >
            <div>
              <FormControl>
                <InputLabel htmlFor="adornment-username">Username</InputLabel>
                <Input
                  id="adornment-username"
                  startAdornment={
                    <InputAdornment position="start">
                      <icon.AccountCircle />
                    </InputAdornment>
                  }
                  value={this.username}
                  onChange={e => {
                    this.username = e.target.value;
                  }}
                />
              </FormControl>
            </div>
            <div>
              <FormControl>
                <InputLabel htmlFor="adornment-password">Password</InputLabel>
                <Input
                  id="adornment-password"
                  type={this.showPassword ? "text" : "password"}
                  value={this.password}
                  onChange={e => {
                    this.password = e.target.value;
                  }}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="Toggle password visibility"
                        onClick={e => {
                          this.showPassword = !this.showPassword;
                        }}
                        onMouseDown={e => {
                          e.preventDefault();
                        }}
                      >
                        {this.showPassword ? (
                          <icon.VisibilityOff />
                        ) : (
                          <icon.Visibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>
            </div>
            <div>
              <Button variant="contained" color="primary" type="submit">
                login
              </Button>
            </div>
          </form>
        </Paper>
      </div>
    );
  }
}

export default Login;
