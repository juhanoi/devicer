import React from "react";
import { Link } from "react-router-dom";
import { observable } from "mobx";
import {
  Grid,
  Typography,
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  TextField,
  Radio,
  RadioGroup,
  FormHelperText,
  FormLabel,
  FormControlLabel,
  FormControl,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "@material-ui/core";
import * as icon from "@material-ui/icons";
import { observer, inject } from "mobx-react";
import axios from "../axios";

@inject("software")
@observer
class Softwares extends React.Component {
  @observable open = false;
  @observable id = null;
  @observable value = "";
  @observable is_operating_system = false;
  software = this.props.software;
  async updateTable() {
    this.software.table = (await axios.get("software")).data.data;
  }
  async componentDidMount() {
    this.updateTable();
  }
  render() {
    return (
      <div>
        <h1>Software</h1>
        <Dialog open={this.open} id={this.id}>
          <DialogTitle>
            {(!this.id ? "Add" : "Modify") + " Software"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText />
            <form>
              <TextField
                name="value"
                label="value"
                onChange={event => {
                  this.value = event.target.value;
                }}
                value={this.value}
              />
              <br />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={this.is_operating_system}
                    onChange={event => {
                      this.is_operating_system = event.target.checked;
                    }}
                  />
                }
                label="is operating system"
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button
              color="secondary"
              variant="outlined"
              onClick={() => {
                this.open = false;
              }}
            >
              cancel
            </Button>
            <Button
              color="primary"
              variant="raised"
              onClick={async () => {
                this.open = false;
                if (!this.id) {
                  await axios.post("software", {
                    value: this.value,
                    is_operating_system: this.is_operating_system
                  });
                } else {
                  await axios.put("software/" + this.id, {
                    value: this.value,
                    is_operating_system: this.is_operating_system
                  });
                }
                await this.updateTable();
              }}
            >
              ok
            </Button>
          </DialogActions>
        </Dialog>
        <Grid container spacing={24}>
          <Grid item xs={2}>
            <Paper>
              <Button
                variant="fab"
                color="primary"
                onClick={() => {
                  this.id = null;
                  this.value = "";
                  this.is_operating_system = false;
                  this.open = true;
                }}
              >
                <icon.Add />
              </Button>
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
            </Paper>
          </Grid>
          <Grid item xs>
            <Paper>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell style={{ width: 0 }} />
                    <TableCell>value</TableCell>
                    <TableCell>is_operating_system</TableCell>
                    <TableCell style={{ width: 0 }} />
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.software.table.map(a => (
                    <TableRow key={a.id}>
                      <TableCell>
                        <Button
                          variant="fab"
                          color="primary"
                          mini
                          onClick={() => {
                            this.id = a.id;
                            this.value = a.value;
                            this.is_operating_system = a.is_operating_system;
                            this.open = true;
                          }}
                        >
                          <icon.ExpandMore />
                        </Button>
                      </TableCell>
                      <TableCell>{a.value}</TableCell>
                      <TableCell>
                        {a.is_operating_system === 1 ? "true" : "false"}
                      </TableCell>
                      <TableCell>
                        <Button
                          variant="fab"
                          color="secondary"
                          mini
                          onClick={async () => {
                            await axios.delete("software/" + a.id);
                            await this.updateTable();
                          }}
                        >
                          <icon.Delete />
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default Softwares;
