import React from "react";
import { Link } from "react-router-dom";
import { observable } from "mobx";
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper
} from "@material-ui/core";
import { observer, inject } from "mobx-react";
import axios from "../axios";

@inject("user")
@observer
class Users extends React.Component {
  user = this.props.user;
  async componentDidMount() {
    let data = (await axios.get("user")).data.data;
    this.user.table = data;
  }
  render() {
    return (
      <div>
        <h1>Users</h1>
        <Button variant="raised" component={Link} to="/user/add">
          add user
        </Button>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>id</TableCell>
                <TableCell>username</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.user.table.map(a => (
                <TableRow key={a.id}>
                  <TableCell>{a.id}</TableCell>
                  <TableCell>{a.username}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

export default Users;
