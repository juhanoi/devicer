import React from "react";
import createHistory from "history/createBrowserHistory";
import { render } from "react-dom";
import { Router } from "react-router-dom";
import { syncHistoryWithStore } from "mobx-react-router";
import { Provider } from "mobx-react";
import store from "./store";
import App from "./App";

let browserHistory = createHistory();
let history = syncHistoryWithStore(browserHistory, store.router);

render(
  <Provider {...store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root")
);
