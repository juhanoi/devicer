let schemas = {
  device: {
    table: "device",
    title: "device",
    type: "object",
    required: ["id", "label", "quantity", "manufacturer_and_model"],
    properties: {
      id: { type: "integer", hide_column: true },
      profile_number: { type: "string" },
      label: { type: "string" },
      device_type_id: { type: "integer" },
      make_model: {
        description: "manufacturer and model",
        type: "string"
      },
      quantity: {
        type: "integer",
        minimum: 0
      },
      serial_number: {
        type: "string",
        hide_column: true
      },
      location: {
        type: "string",
        hide_column: true
      },
      additional_info: {
        type: "string",
        hide_column: true
      },
      ip_address: {
        type: "string",
        hide_column: true
      },
      cpu: { type: "string", hide_column: false },
      ram: { type: "string", hide_column: true },
      hdd: {
        type: "string",
        hide_column: true
      },
      gpu: { type: "string" },
      purchase_store: { type: "string", hide_column: true },
      purchase_date: { type: "string", hide_column: true }
    }
  },
  device_type: {
    table: "device_type",
    title: "device type",
    type: "object",
    properties: {
      id: { type: "integer" },
      value: { type: "string" }
    }
  },
  software: {
    table: "software",
    title: "software",
    type: "object",
    properties: {
      id: { type: "integer" },
      software: { type: "string" },
      is_operating_system: { type: "boolean" }
    }
  },
  device_software: {
    table: "device_software",
    title: "device software",
    type: "object",
    properties: {
      id: { type: "integer" },
      license_key: { type: "string" },
      info: { type: "string" }
    }
  },
  user: {
    table: "user",
    title: "user",
    type: "object",
    properties: {
      id: { type: "integer" },
      username: { type: "string" },
      password: { type: "string" }
    }
  }
};

export default schemas;
