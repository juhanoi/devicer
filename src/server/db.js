import path from "path";
import knex from "knex";

let db = knex({
  client: "sqlite3",
  connection: {
    filename: path.resolve(__dirname, "../../dev.sqlite3")
  },
  debug: true,
  useNullAsDefault: true
});

export default db;
