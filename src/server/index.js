import dotenv from "dotenv";
dotenv.config();
import path from "path";
import express from "express";
import cors from "cors";
import session from "express-session";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import router from "./router";

let staticPath = path.resolve(__dirname, "../../static");
let app = express();

if (process.env.NODE_ENV !== "production") {
  let webpack = require("webpack");
  let config = require("../../webpack.config.js");
  let compiler = webpack(config);
  let hotMiddleware = require("webpack-hot-middleware")(compiler, {
    heartbeat: 1000
  });
  compiler.hooks.compilation.tap("html-webpack-plugin-after-emit", () => {
    hotMiddleware.publish({
      action: "reload"
    });
  });
  app.use(
    require("webpack-dev-middleware")(compiler, {
      publicPath: config.output.publicPath
    })
  );
  app.use(hotMiddleware);
}

app.use(cors());
app.use(
  session({
    secret: process.env.sessionSecret,
    cookie: { maxAge: 999999 },
    resave: false,
    saveUninitialized: false
  })
);
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/api", router);
app.use("/static", express.static(staticPath));
app.get("*", (req, res) => {
  res.sendFile(path.resolve(staticPath, "index.html"));
});
app.listen(process.env.appPort);
