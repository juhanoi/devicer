import express from "express";
import passport from "passport";
import { Strategy as LocalStrategy } from "passport-local";
// import bcrypt from "bcrypt";
import db from "./db";
import schemas from "../common/schemas";

let router = express.Router();

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  try {
    let user = await db("user")
      .where({ id })
      .first();
    return done(null, user);
  } catch (e) {
    done(e);
  }
});

passport.use(
  new LocalStrategy(async (username, password, done) => {
    try {
      let user = await db("user")
        .where({ username })
        .first();
      if (!user) {
        return done(null, false, { message: "wrong username" });
      }
      // FIXME
      // await bcrypt.compare(password, user.password);
      if (password != user.password) {
        return done(null, false, { message: "wrong password" });
      }
      return done(null, user);
    } catch (e) {
      return done(e);
    }
  })
);

router.use(passport.initialize());
router.use(passport.session());

router.use((req, res, next) => {
  res.reply = (body = {}, error = null) => {
    return res.json({ data: body, error: error });
  };
  next();
});

router.post("/login", passport.authenticate("local"), (req, res) => {
  res.reply();
});

router.get("/auth", (req, res) => {
  if (req.user) {
    res.reply();
  } else {
    res.reply({}, "user is not authenticated");
  }
});

router.use((req, res, next) => {
  if (req.user) {
    return next();
  }
  return res.status(401).reply({}, "not authorized");
});

router.post("/logout", (req, res) => {
  req.logout();
  res.reply();
});

let makeTableRoutes = schema => {
  let table = schema.table;
  let path = "/" + table;

  router.get(path, async (req, res) => {
    try {
      let query = await db
        .select(...Object.keys(schema.properties))
        .from(table);
      res.reply(query);
    } catch (e) {
      res.status(500).reply({}, e);
    }
  });

  router.post(path, async (req, res) => {
    try {
      let query = await db(table).insert(req.body);
      res.reply();
    } catch (e) {
      res.status(500).reply({}, e);
    }
  });

  router.put(path + "/:id", async (req, res) => {
    try {
      let query = await db(table)
        .where({ id: req.params.id })
        .update(req.body);
      res.reply();
    } catch (e) {
      res.status(500).reply({}, e);
    }
  });

  router.delete(path + "/:id", async (req, res) => {
    try {
      let query = await db(table)
        .where({ id: req.params.id })
        .del();
      res.reply();
    } catch (e) {
      res.status(500).reply({}, e);
    }
  });
};

makeTableRoutes(schemas.device);
makeTableRoutes(schemas.device_type);
makeTableRoutes(schemas.software);
makeTableRoutes(schemas.device_software);
makeTableRoutes(schemas.user);

export default router;
