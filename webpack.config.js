import path from "path";
import Dotenv from "dotenv-webpack";
import webpack from "webpack";
import HtmlWebPackPlugin from "html-webpack-plugin";

module.exports = {
  mode: "development",
  entry: [
    "babel-polyfill",
    "webpack-hot-middleware/client?reload=true",
    "./src/client/index.js"
  ],
  output: {
    path: path.resolve(__dirname, "./static"),
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: true }
          }
        ]
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new Dotenv({
      path: "./.env",
      safe: true
    }),
    new HtmlWebPackPlugin({
      template: "./src/client/index.html"
    })
  ]
};
